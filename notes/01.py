primNumber = []
print("请输入正整数：")
num = input()
if num.isdigit():
    num = eval(num)
    if num > 2:
        for i in range(2, num):
            flag = True
            for k in range(2, int(i/2) + 1):
                if i % k == 0:
                    flag = False
                    break
            if flag:
                primNumber.append(i)
        print(primNumber)
    else:
        print("输入是数字不能小于2！")
else:
    print("输入了非法字符！")
